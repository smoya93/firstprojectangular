import { LoginInterface } from "../api/interfaces/login-interface";
export class UserModel {
    id: string;
    firstName: string;
    lastName: string;
    email: string;
    userName: string

    constructor(loginInterface: LoginInterface){
        this.id = loginInterface.id;
        this.firstName = loginInterface.user.firstName;
        this.lastName = loginInterface.user.lastName;
        this.email = loginInterface.user.email;
        this.userName = loginInterface.user.userName;
    }
}
