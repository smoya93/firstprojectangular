import { TestBed } from '@angular/core/testing';
import { SpinnerService } from './spinner.service';

describe('SpinnerService', () => {
  let service: SpinnerService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        ],
        providers: [
          SpinnerService
        ],
    });
    service = TestBed.inject(SpinnerService);
  });
  
  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('#setLoading should be false', () => {
    expect(service.setLoading(false)).toBe();
  });
});
