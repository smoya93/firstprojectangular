import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { LoginResponseInterface } from '../api/interfaces/login-response-interface';
import { LoginUserModel } from '../pages/login/models/login-user-model';
import { HttpClient} from '@angular/common/http';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  constructor(private httpClient: HttpClient) { }

  validateUser(loginUser: LoginUserModel): Observable<LoginResponseInterface> {
    return this.httpClient.get<LoginResponseInterface>(`${environment.apiUrl}/loginResponse`);
   }
}
