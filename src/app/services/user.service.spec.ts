import { TestBed } from '@angular/core/testing';
import {
  HttpClientTestingModule,
  HttpTestingController,
} from '@angular/common/http/testing';

import { UserService } from './user.service';
import { LoginResponseInterface } from '../api/interfaces/login-response-interface';
import { environment } from 'src/environments/environment';

describe('UserService', () => {
  let userService: UserService;
  let httpTestingController: HttpTestingController;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [],
    });
    userService = TestBed.inject(UserService);
    httpTestingController = TestBed.inject(HttpTestingController);
  });

  it('should be created', () => {
    expect(userService).toBeTruthy();
  });

  it('be able to retrieve user from the API GET', () => {
    const dummyUser: LoginResponseInterface = {
        data: {
          id: "3333-38441-54d31-ddd7-9bae",
          token: "dasdndasjassssssk989423njkdas87ad37dnasmndasbhdasjasjd8334bsd",
          user: {
            id: "bi0594",
            firstName: "Sergio",
            lastName: "Moya Rodríguez",
            email: "smoya1993@hotmail.com",
            userName: "smoya"
          },
          roles: {
            role: "Administrator"
          },
          expires: new Date(0),
          refreshToken: "dasdndasjassssssk989423njkdas87ad37dnasmndasbhdasjasjd8334bsd"
        },
        successfull: true,
        message: "Login successfull",
        statusCode: 200
    };

    userService
      .validateUser({
        email: '',
        password: '',
        reminder: false,
      })
      .subscribe((userResponse) => {
        expect(userResponse).toEqual(dummyUser);
      });
    const request = httpTestingController.expectOne(
      `${environment.apiUrl}/loginResponse`
    );
    expect(request.request.method).toBe('GET');
    request.flush(dummyUser);
  });

  afterEach(() => {
    httpTestingController.verify();
  });
});
