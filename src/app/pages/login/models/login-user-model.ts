/* Se puede hacer que extienda de UserModel y que obtenga sus atributos */
export class LoginUserModel {
    email: string;
    password: string;
    reminder: boolean;

    constructor(){
        this.email = "";
        this.password = "";
        this.reminder = false;
    }
}
