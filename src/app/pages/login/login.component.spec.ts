import { HttpClient, HttpClientModule } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { async, ComponentFixture, inject, TestBed } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { of } from 'rxjs';
import { LoginResponseInterface } from 'src/app/api/interfaces/login-response-interface';
import { UserService } from 'src/app/services/user.service';

import { LoginComponent } from './login.component';
import { LoginUserModel } from './models/login-user-model';

describe('LoginComponent', () => {
  let loginComponent: LoginComponent;
  let fixture: ComponentFixture<LoginComponent>;
  let element;
  let userService: UserService;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        LoginComponent
      ],
      providers: [
        UserService
      ],
      imports: [HttpClientModule]
    }).compileComponents();
  }));

  beforeEach(inject([UserService], (s: UserService) => {
    userService = s;
    fixture = TestBed.createComponent(LoginComponent);
    loginComponent = fixture.componentInstance;
    element = fixture.nativeElement;
  }));

  it("should retrieve a user in login", async(() => {
    const dummyValUser: LoginUserModel = {
      email: 'smoya1993@hotmail.com',
      password: 'kIDASJkdasl021',
      reminder: true,
    };
    const dummyUser: LoginResponseInterface = {
      data: {
        id: "003333-38441-54d31-ddd7-9bae",
        token: "dasdndasjassssssk989423njkdas87ad37dnasmndasbhdasjasjd8334bsd",
        user: {
          id: "bi0594",
          firstName: "Sergio",
          lastName: "Moya Rodríguez",
          email: "smoya1993@hotmail.com",
          userName: "smoya",
        },
        roles: {
          role: "Administrator",
        },
        expires: new Date(0),
        refreshToken:
          "dasdndasjassssssk989423njkdas87ad37dnasmndasbhdasjasjd8334bsd",
      },
      successfull: true,
      message: "Login successfull",
      statusCode: 200,
    };
    spyOn(userService, 'validateUser').and.returnValue(of(dummyUser));
    loginComponent.loginEvent(dummyValUser);
    fixture.detectChanges();
    expect(loginComponent.user.id).toEqual(dummyUser.data.id);
  }));
});
