import { ComponentFixture, TestBed } from '@angular/core/testing';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { LoginUserModel } from '../../models/login-user-model';

import { LoginFormComponent } from './login-form.component';

describe('LoginFormComponent', () => {
  let component: LoginFormComponent;
  let fixture: ComponentFixture<LoginFormComponent>;
  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [FormsModule, ReactiveFormsModule],
      declarations: [LoginFormComponent],
    });
    // create component and test fixture
    fixture = TestBed.createComponent(LoginFormComponent);
    // get test component from the fixture
    component = fixture.componentInstance;
    component.ngOnInit();
  });

  it('form invalid when empty', () => {
    expect(component.formLogin.valid).toBeFalsy();
  });

  it('email field validity', () => {
    let email = component.formLogin.controls['email'];
    expect(email.valid).toBeFalsy();
    // Email field is required
    expect(email.errors?.required).toBeTruthy();

    // Set email to something
    email.setValue('test');
    expect(email.errors?.required).toBeFalsy();
    expect(email.errors?.pattern).toBeTruthy();

    // Set email to something correct
    email.setValue('test@example.com');
    expect(email.errors?.required).toBeFalsy();
    expect(email.errors?.pattern).toBeFalsy();
  });

  it('password field validity', () => {
    let errors = {};
    let password = component.formLogin.controls['password'];

    // Email field is required
    expect(password.errors?.required).toBeTruthy();

    // Set email to something
    password.setValue('12');
    expect(password.errors?.required).toBeFalsy();
    expect(password.errors?.minlength).toBeTruthy();

    // Set email to something correct
    password.setValue('123456789');
    errors = password.errors || {};
    expect(password.errors?.required).toBeFalsy();
    expect(password.errors?.minlength).toBeFalsy();
  });

  it('submitting a form emits a user', () => {
    expect(component.formLogin.valid).toBeFalsy();
    component.formLogin.controls['email'].setValue('test@test.com');
    component.formLogin.controls['password'].setValue('123456789');
    expect(component.formLogin.valid).toBeTruthy();

    let user: LoginUserModel = new LoginUserModel();
    // Subscribe to the Observable and store the user in a local variable.
    component.loginEvent.subscribe((value) => (user = value));

    // Trigger the login function
    component.clickLoginForm();

    // Now we can check to make sure the emitted value is correct
    expect(user.email).toBe('test@test.com');
    expect(user.password).toBe('123456789');
    expect(user.reminder).toBe(false);
  });
});
