import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { LoginUserModel } from '../../models/login-user-model';

/**
 * LoginFormComponent: Componente que carga el formulario del login. estará formado por dos campos de tipo string (correo y contraseña) y un campo de tipo check para
 * mantener iniciado el login
 */

@Component({
  selector: 'app-login-form',
  templateUrl: './login-form.component.html',
  styleUrls: ['./login-form.component.scss'],
})

export class LoginFormComponent implements OnInit {
  formLogin!: FormGroup;
  @Output()
  loginEvent = new EventEmitter<LoginUserModel>();
  submitted: boolean = false;

  constructor(private formbuilder: FormBuilder) {}

  ngOnInit(): void {
    this.formLogin = this.formbuilder.group({
      email: ['', [Validators.required, Validators.pattern("[^ @]*@[^ @]*")]],
      password: ['', [Validators.required, Validators.minLength(5)]],
      reminder: false,
    });
  }

  get f() {
    return this.formLogin.controls;
  }

  clickLoginForm() {
    this.submitted = true;
    if (this.formLogin.valid) {
      this.loginEvent.emit({
        
        email: this.formLogin.value.email,
        password: this.formLogin.value.password,
        reminder: this.formLogin.value.reminder
      });
    }
  }
}
