import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { LoginResponseInterface } from 'src/app/api/interfaces/login-response-interface';
import { UserService } from 'src/app/services/user.service';
import { LoginUserModel } from './models/login-user-model';
import { delay, take } from 'rxjs/operators';
import { Router } from '@angular/router';
import { HttpErrorResponse } from '@angular/common/http';
import { UserModel } from 'src/app/models/user-model';
import { SpinnerService } from 'src/app/services/spinner.service';


/**
 * Método que comprobará si el usuario es válido, mediante una llamada al servicio y
 * obtendrá los datos del usuario.
 */

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  loginResponse$!: Observable<LoginResponseInterface>;
  user!: UserModel;
  loginResponseInterface!: LoginResponseInterface;

  constructor(private userService: UserService, private spinnerService: SpinnerService) { }

  ngOnInit(): void {
  }

  loginEvent(loginUser: LoginUserModel): void {
    this.spinnerService.setLoading(true);
    this.userService.validateUser(loginUser).subscribe(
      (resp: LoginResponseInterface) => {
        this.user = new UserModel(resp.data);
        let messageLog: string = (resp.statusCode === 200) ? `El usuario: ${resp.data.user.userName} se ha logeado con éxito` : `Error en llamada. statusCode = ${resp.statusCode}`;
        console.log(messageLog);
        console.log(this.user);
        this.spinnerService.setLoading(false);
      },
      (error: HttpErrorResponse) => {
        this.spinnerService.setLoading(false);
        console.error(
          `Se ha producido un error en la llamada al servicio de loginEvent(): ${error.message}`
        );
      }
    );
  }
}
