import { LoginInterface } from "./login-interface";

export interface LoginResponseInterface {
    data: LoginInterface,
    successfull: boolean,
    message: string;
    statusCode: number
}
