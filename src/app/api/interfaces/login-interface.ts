import { RoleInterface } from "./role-interface";
import { UserInterface } from "./user-interface";

export interface LoginInterface {
    id: string,
    token: string,
    user: UserInterface,
    roles: RoleInterface,
    expires: Date,
    refreshToken: string
}
